class BodyHandler:
    """Handles every body."""
    def __init__(self, b = []):
        self.bodys = b

    def removeBody(self, body):
        self.bodys.remove(body)

    def addBody(self, body):
        self.bodys.append(body)

    def scaleSpeed(self, scalar):
        for b in self.bodys:
            b.scaleSpeed(scalar)
    
    def update(self):
        """Merges the bodys to merge, returns a list with the position of every body."""
        merge = self.getForces()

        for entry in merge:
            self.merge(entry[0], entry[1])

        return list(map(lambda b: b.update(), self.bodys))
    
    def getForces(self):
        """Calculates forces between all bodys, returns a list of bodys to merge."""
        merge = []
        for i in range(len(self.bodys)):
            for j in range(i+1, len(self.bodys)):
                body1, body2 = self.bodys[i], self.bodys[j]
                f = body1.forceToBody(body2)
                if f == -1:
                    merge.append((body1, body2))
                else:
                    body1.addForce(f, 1)
                    body2.addForce(f, -1)

        return merge

    def merge(self, b1, b2):
        """Merges two bodys and conserves mass and inertia."""
        self.bodys.remove(b1)
        b2.velocity = tuple(map(sum, zip([b1.mass*x for x in b1.velocity], [b2.mass*x for x in b2.velocity])))
        b2.mass = b2.mass + b1.mass
