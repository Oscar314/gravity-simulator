import pygame
from bodyHandler import BodyHandler
from body import Body
from pygame_textinput import InputBox
from Constants import Constants as C

class Main:
    def __init__(self):
        self.bodyHandler = BodyHandler(self.createBodys())
        self.screen = pygame.display.set_mode((750, 750))
        self.usrInput = InputBox()
        pygame.init()

    def createBodys(self):
        b1 = Body(C.earthMass, (C.au, 0), (C.earthV*C.timeScale, 0), 15)
        b2 = Body(C.sunMass, (C.au, C.au), (0, 0), 15)
        b3 = Body(C.sunMass, (0, C.au), (0, C.earthV*C.timeScale*0.5), 15)
        return([b1, b2, b3])

    def handleEvent(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                return False
            scaleChange = self.usrInput.handle_event(event)
            if scaleChange != None:
                self.setTimeScale(int(scaleChange))
        return True
                
    def setTimeScale(self, newScale):
        scalar = newScale/(C.timeScale)
        self.bodyHandler.scaleSpeed(scalar/100)
        C.timeScale = newScale/100

    def updateScreen(self, bodyPositions, timeSinceStart):
            self.screen.fill(C.white)
            for position in bodyPositions:
                pygame.draw.circle(self.screen, C.red , tuple(map(lambda x: int(x/C.sizeScale), position)), 10)

            self.usrInput.draw(self.screen, timeSinceStart)
            pygame.display.update()

    def run(self, running):
        clock = pygame.time.Clock()
        timeSinceStart = 0
        while running:
            clock.tick(100)
            bodyPositions = self.bodyHandler.update()
            timeSinceStart += (C.timeScale)
            running = self.handleEvent()
            self.updateScreen(bodyPositions, timeSinceStart)

main = Main()
main.run(True)

