import pygame
from Constants import Constants as C
pygame.init()

class InputBox:
    def __init__(self):
        self.font = pygame.font.Font('freesansbold.ttf', 32)
        self.text = "1"

    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                return self.text
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            elif event.key == pygame.K_SPACE:
                self.text = "1"
                return self.text
            else:
                self.text += event.unicode

    def draw(self, screen, runTime):
        self.drawTime(screen, runTime)
        self.drawMultiplier(screen)

    def drawTime(self, screen, runTime):
        time = self.font.render("Seconds since start: " + str(int(runTime)), True, C.green, C.blue) 
        timeBox = time.get_rect()
        timeBox.midleft = (0, 700) 
        screen.blit(time, timeBox) 

    def drawMultiplier(self, screen):
        multiplier = self.font.render("Time multiplier: " + self.text, True, C.green, C.blue) 
        multiplierBox = multiplier.get_rect()
        multiplierBox.midleft = (0, 732) 
        screen.blit(multiplier, multiplierBox) 




