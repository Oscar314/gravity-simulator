from Constants import Constants as C

class Body:
    """Represents a body in any dimension"""
    def __init__(self, mass, pos, velocity, radius):
        self.mass = mass
        self.pos = pos
        self.velocity = velocity
        self.radius = radius
        self.force = tuple(map(lambda _ : 0, pos))

    def scaleSpeed(self, scalar):
        self.velocity = tuple(map(lambda x: x*scalar, self.velocity))

    def addForce(self, f, direction):
        """Adds a force with direction beeing 1 or -1"""
        self.force = tuple(map(lambda f1, f2: (f1*direction)+f2, f, self.force))

    def update(self):
        """Sets new velocity and  position using self.force."""
        acceleration = map(lambda x: x/self.mass, self.force)
        self.velocity = tuple(map(sum, zip(acceleration , self.velocity)))

        self.pos = tuple(map(sum, zip(self.pos, self.velocity)))
        self.force = tuple(map(lambda _ : 0, self.force))

        return self.pos

    def forceToBody(self, other):
        """Returns the force to other as a vector using direction or -1 if colision."""
        r = self.distanceToBody(other)
        if(r < self.radius + other.radius):
            return -1
        else:
            force = (C.timeScale**2)* C.G * self.mass * other.mass / r ** 2
            return tuple(map(lambda x: x*force, self.directionToBody(other)))

    def distanceToBody(self, other):
        """Returns the distance to other."""
        return sum(map(lambda a,b: (a-b)**2, self.pos, other.pos))**0.5
        
    def directionToBody(self, other):
        """Returns a normed direction to other."""
        distance = self.distanceToBody(other)
        return tuple(map(lambda a,b: (a-b)/distance, other.pos, self.pos))