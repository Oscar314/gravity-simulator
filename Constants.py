
class Constants:
    earthMass = 5.97237*10**24
    sunMass = 1.9885*10**30
    au = 149600*10**6
    earthV = 29780 
    timeScale = 1/100 #365*24*60*60/100
    sizeScale = 4*10**8
    G = 6.674 * 10 ** (-11)
    white = (255,255,255)
    red = (255,0,0)
    white = (255, 255, 255) 
    green = (0, 255, 0) 
    blue = (0, 0, 128) 
